## NixOS WSL2 Install

- Get the [latest
  release](https://github.com/LGUG2Z/nixos-wsl-starter/releases)
- Install it (tweak the command to your desired paths):

```powershell
wsl --import NixOS .\NixOS\ .\nixos-wsl.tar.gz --version 2
```

- Enter the distro:

```powershell
wsl -d NixOS
```

- Get a copy of this repo (you'll probably want to fork it eventually):

```bash
git clone https://gitlab.com/Sailord/dotfiles.git /tmp/configuration
cd /tmp/configuration
```

- Apply the wsl profile configuration and shutdown the WSL2 VM

```bash
sudo nixos-rebuild switch --flake /tmp/configuration#achlys && sudo shutdown -h now
```

- Reconnect to the WSL2 VM

```bash
wsl -d NixOS
```
And you should be good to go!

### win32yank

There have been some recent changes in WSL2 that make running `win32yank`
within WSL2 very slow. You should install this on Windows then add it to your `PATH`

- Install scoop by running the following command:

```powershell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
```
- Install win32yank

```powershell
scoop install win32yank
```
- Then add it to your `PATH`

```nix
{
    programs.fish = {
      interactiveShellInit = ''
        fish_add_path --append /mnt/c/Users/<Your Windows Username>/scoop/apps/win32yank/0.1.1
      '';
    };
}
```
