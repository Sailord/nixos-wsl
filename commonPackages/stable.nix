{ pkgs, ... }:
with pkgs.stable;
[
  onefetch
  neofetch

  # core languages
  lua
  nodejs
  python3
  typescript

  # treesitter
  # tree-sitter

  # language servers
  ccls # c / c++
  nil # nix

  # formatters and linters
  deadnix # nix
]
