{ pkgs, ... }:
with pkgs;
[
  # NOTE: select your core binaries that you always want on the bleeding-edge
  bat
  bottom
  dive
  coreutils
  curl
  du-dust
  duf
  dive
  fd
  findutils
  fx
  htop
  jq
  killall
  mosh
  nix-tree
  procs
  ripgrep
  sd
  tmux
  tree
  unzip
  wget
  zip
  ranger
  terraform
  k9s
  gnumake
  tldr
  navi
  opentofu
  lnav
  ansible
  molecule
  kubectl
  kconf
  sops
  age
  wslu
  pgcli
  doggo

  devbox
  devenv

  # git
  pre-commit
  gitmoji-cli
  git
  git-crypt
  git-graph
  glab
  go_1_23
  # Azure
  azure-cli
]
