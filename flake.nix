{
  description = "NixOS configuration";

  inputs.nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  inputs.home-manager.url = "github:nix-community/home-manager";
  inputs.home-manager.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nur.url = "github:nix-community/NUR";

  inputs.nixos-wsl.url = "github:nix-community/NixOS-WSL";
  inputs.nixos-wsl.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nix-index-database.url = "github:Mic92/nix-index-database";
  inputs.nix-index-database.inputs.nixpkgs.follows = "nixpkgs";

  inputs.jeezyvim.url = "github:Sail0rd/JeezyVim";

  outputs =
    inputs:
    with inputs;
    let
      secrets = builtins.fromJSON (builtins.readFile "${self}/secrets.json");

      nixpkgsWithOverlays = with inputs; rec {
        config = {
          allowUnfree = true;
          permittedInsecurePackages = [
            # NOTE: add any insecure packages you absolutely need here
          ];
        };
        overlays = [
          nur.overlays.default
          jeezyvim.overlays.default
          (_final: prev: {
            # this allows us to reference pkgs.unstable
            stable = import nixpkgs-stable {
              inherit (prev) system;
              inherit config;
            };
          })
        ];
      };

      configurationDefaults = args: {
        nixpkgs = nixpkgsWithOverlays;
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.backupFileExtension = "hm-backup";
        home-manager.extraSpecialArgs = args;
      };

      argDefaults = {
        inherit
          secrets
          inputs
          self
          nix-index-database
          ;
        channels = {
          inherit nixpkgs nixpkgs-stable;
        };
      };

      mkNixosConfiguration =
        {
          system ? "x86_64-linux",
          hostname,
          username,
          email,
          args ? { },
          modules,
        }:
        let
          specialArgs = argDefaults // { inherit hostname username email; } // args;
        in
        nixpkgs.lib.nixosSystem {
          inherit system specialArgs;
          modules = [
            (configurationDefaults specialArgs)

            home-manager.nixosModules.home-manager
          ] ++ modules;
        };
    in
    {
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;

      nixosConfigurations."L-FGKDSL3" = mkNixosConfiguration {
        hostname = "L-FGKDSL3";
        username = "mathis"; # NOTE: replace with your own username!
        email = "mathis.guilbaud@avisto.com";
        modules = [
          nixos-wsl.nixosModules.wsl
          ./wsl.nix
        ];
      };

      nixosConfigurations.achlys = mkNixosConfiguration {
        hostname = "achlys";
        username = "Sailord"; # NOTE: replace with your own username!
        email = "mathis.guilbaud@epita.fr";
        modules = [
          nixos-wsl.nixosModules.wsl
          ./wsl.nix
        ];
      };
    };
}
