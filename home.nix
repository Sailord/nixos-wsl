{
  # NOTE: uncomment the next line if you want to reference your GitHub/GitLab access tokens and other secrets
  # secrets,
  pkgs,
  username,
  nix-index-database,
  ...
}:
let
  jeezyvim = import programs/jeezyvim.nix { inherit pkgs; };
  unstable-packages = import commonPackages/unstable.nix { inherit pkgs; };
  stable-packages = import commonPackages/stable.nix { inherit pkgs; };
in
{
  imports = [
    nix-index-database.hmModules.nix-index
    ./modules/hm/sshs.nix
    ./modules/hm/trackwarrior.nix

    ./programs/fish.nix
    ./programs/git.nix
    ./programs/helm.nix
    ./programs/lazygit.nix
    ./programs/starship.nix

    ./services/sshs.nix
  ];

  home.stateVersion = "22.11";

  home = {
    username = "${username}";
    homeDirectory = "/home/${username}";

    sessionVariables.EDITOR = "nvim";
    sessionVariables.SHELL = "/etc/profiles/per-user/${username}/bin/fish";
    sessionVariables.BROWSER = "wslview";
  };

  home.packages =
    stable-packages
    ++ unstable-packages
    ++ jeezyvim
    ++
      # NOTE: you can add anything else that doesn't fit into the above two lists in here
      [
        # pkgs.some-package
        # pkgs.unstable.some-other-package
      ];

  services = {
    trackwarrior = {
      enable = true;
    };
  };

  programs = {
    home-manager.enable = true;
    nix-index.enable = true;
    # nix-index.enableZshIntegration = true;
    nix-index.enableFishIntegration = true;
    nix-index-database.comma.enable = true;

    # NOTE: disable whatever you don't want
    fzf.enable = true;
    fzf.enableFishIntegration = true;
    #fzf.enableZshIntegration = true;
    lsd.enable = true;
    lsd.enableAliases = true;
    zoxide.enable = true;
    zoxide.enableFishIntegration = true;
    #zoxide.enableZshIntegration = true;
    broot.enable = true;
    broot.enableFishIntegration = true;
    #broot.enableZshIntegration = true;

    direnv.enable = true;
    #direnv.enableZshIntegration = true;
    #direnv.enableFishIntegration = true;
    direnv.nix-direnv.enable = true;

  };
}
