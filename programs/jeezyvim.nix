{ pkgs, ... }:
with pkgs;
[
  (jeezyvim.extend {
    plugins = {
      none-ls.sources.diagnostics.sqruff = {
        enable = true;
        settings = {
          extra_args = [
            "--dialect"
            "postgres"
          ];
        };
      };
      copilot-vim = {
        enable = true;
        settings.filetypes = {
          "*" = true;
        };
      };
    };
    extraPlugins = with pkgs.vimPlugins; [ vim-easymotion ];
  })
]
