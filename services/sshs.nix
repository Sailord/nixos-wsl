{ config, pkgs, ... }:
{
  services.sshs = {
    enable = true;
    sshKey = "${config.home.homeDirectory}/.ssh/id_ed25519";
    hostConfigs = {
      "talenthub-preprod" = {
        hostName = "talenthub-preprod.devops-svc-ag.com";
        user = "devops";
      };
      "talenthub" = {
        hostName = "talenthub.devops-svc-ag.com";
        user = "devops";
      };
      "bastion-QA-edf-enr" = {
        hostName = "20.56.151.205";
        user = "espcli";
      };
      "k8s-master" = {
        hostName = "10.10.69.20";
        user = "ubuntu";
      };
      "k8s-worker-1" = {
        hostName = "10.10.69.30";
        user = "ubuntu";
      };
      "k8s-worker-2" = {
        hostName = "10.10.69.31";
        user = "ubuntu";
      };
      "k8s-worker-3" = {
        hostName = "10.10.69.32";
        user = "ubuntu";
      };
    };
  };
}
